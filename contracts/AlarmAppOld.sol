pragma solidity ^0.4.21;
import './zeppelin/lifecycle/Killable.sol';

contract AlarmApp is Killable {

  /***** STORAGE ******/

  struct User {
    address userAddress;
    uint totalAmountDeposited;
    uint createdAtBlockNumber;
    uint createdAtTimestamp;
  }

  struct Deposit {
    bytes32 id;
    address userAddress;
    uint amount;
    uint createdAtBlockNumber;
    uint createdAtTimestamp;
  }

  address[] public userAddresses;
  mapping(address => User) public users;
  bytes32[] public depositIds;
  mapping(address => bytes32[]) public depositIdsByUserAddress;
  mapping(bytes32 => Deposit) public deposits;

  /***** EVENTS ******/
  event UserCreated(address indexed _userAddress,
                    address _newUserAddress);

  event DepositMade(address indexed _userAddress,
                    bytes32 _depositId,
                    address _depositor,
                    uint _updatedAmountDeposited,
                    uint _amount);

  /***** GETTERS ******/

  function getUserAddresses() public view onlyOwner returns (address[]) {
    return userAddresses;
  }

  function getDepositIds() public view returns (bytes32[]) {
    return depositIds;
  }

  function getDepositIdsByUserAddress(address userAddress)
    public view returns (bytes32[]) {
    return depositIdsByUserAddress[userAddress];
  }

  /***** PUBLIC FUNCTIONS ******/

  // Make deposit
  function makeDeposit() public payable {
    require(msg.value > 0);
    if (users[msg.sender].createdAtTimestamp == 0) { createUser(); }
    users[msg.sender].totalAmountDeposited += msg.value;
    createDeposit();
  }

  function adminWithdraw(uint amount) public onlyOwner {
    msg.sender.transfer(amount);
  }

  /***** PRIVATE FUNCTIONS ******/

  // Create Deposit
  function createDeposit() private {
    Deposit memory newDeposit;
    bytes32 id = keccak256(msg.sender, depositIds.length, block.timestamp);
    // verify that id doesn't already exist
    require(deposits[id].createdAtTimestamp == 0);
    newDeposit.id = id;
    newDeposit.userAddress = msg.sender;
    newDeposit.amount = msg.value;
    newDeposit.createdAtBlockNumber = block.number;
    newDeposit.createdAtTimestamp = block.timestamp;
    depositIds.push(newDeposit.id);
    deposits[newDeposit.id] = newDeposit;
    depositIdsByUserAddress[msg.sender].push(newDeposit.id);
    emit DepositMade(msg.sender, newDeposit.id, msg.sender,
                     users[msg.sender].totalAmountDeposited, msg.value);
  }

  // Create User
  function createUser() private {
    User memory newUser;
    newUser.userAddress = msg.sender;
    newUser.totalAmountDeposited = uint(0);
    newUser.createdAtBlockNumber = block.number;
    newUser.createdAtTimestamp = block.timestamp;
    users[msg.sender] = newUser;
    userAddresses.push(msg.sender);
    emit UserCreated(msg.sender, msg.sender);
  }

}
