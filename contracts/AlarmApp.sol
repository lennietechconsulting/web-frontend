pragma solidity ^0.4.21;
import './zeppelin/lifecycle/Killable.sol';

contract AlarmApp is Killable {

  event DepositMade(
    address indexed _accountAddressIndex,
    address _accountAddress,
    uint _amount
  );

  function makeDeposit() public payable {
    require(msg.value > 0);
    emit DepositMade(msg.sender, msg.sender, msg.value);
  }

}
