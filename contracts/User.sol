pragma solidity ^0.4.21;

library User {

  struct UserStruct {
    uint balance;
    uint createdAt;
    uint updatedAt;
  }

  event UserCreated(address _userAddress);

  event DepositMade(address indexed _userAddress,
                    address _depositor,
                    uint _updatedBalance,
                    uint _amount);

  function create(UserStruct storage self) public {
    self.balance = uint(0);
    self.createdAt = block.timestamp;
    self.updatedAt = block.timestamp;
    emit UserCreated(msg.sender);
  }

  function makeDeposit(UserStruct storage self) public {
    self.balance += msg.value;
    emit DepositMade(msg.sender, msg.sender, self.balance, msg.value);
  }

}
