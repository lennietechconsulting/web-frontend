const AlarmApp = artifacts.require("./AlarmApp.sol");
const Helpers = require('./alarmAppHelpers')

contract('AlarmApp', async (accounts) => {

  Helpers.accounts = accounts
  AlarmApp.deployed().then((instance) => { Helpers.instance = instance })

  ////////////////////////////////////////////
  // AlarmApp
  ////////////////////////////////////////////

  // makeDeposit
  it("is public", Helpers.makeDepositIsPublic)
  it("is payable", Helpers.makeDepositIsPayable)
  it("reverts if value is less or equal to 0", Helpers.makeDepositRevertsWhenZero)
  it("emits DepositMade event with correct params", Helpers.makeDepositEmitsEvent)
})

contract('Killable', async (accounts) => {

  Helpers.accounts = accounts
  AlarmApp.deployed().then((instance) => { Helpers.instance = instance })

  ////////////////////////////////////////////
  // Killable
  ////////////////////////////////////////////

  // make deposit
  it("add deposit", Helpers.callMakeDeposit)

  // killIReallyWa1ntToDoThis
  it("reverts if caller is not owner", Helpers.killRevertsIfNotOwner)
  it("removes contract, code, data and sends all contract funds to owner",
     Helpers.killRemovesContractAndSendsFundsToOwner)
})

contract('Ownable', async (accounts) => {

  Helpers.accounts = accounts
  AlarmApp.deployed().then((instance) => { Helpers.instance = instance })

  ////////////////////////////////////////////
  // Ownable
  ////////////////////////////////////////////

  // make deposit
  it("add deposit", Helpers.callMakeDeposit)

  // onlyOwner
  // change this to ownerWithdraw
  it("onlyOwner reverts if caller is not owner",
    Helpers.ownerWithdrawRevertsIfNotOwner)

  // ownerWithdraw
  it("ownerWithdraw reverts if caller is not owner",
    Helpers.ownerWithdrawRevertsIfNotOwner)
  it("ownerWithdraw sends amount specified to owner",
    Helpers.ownerWithdrawWithdrawsCorrectAmount)

  // owner
  it("is public", Helpers.ownerHasGetFunction)
})
