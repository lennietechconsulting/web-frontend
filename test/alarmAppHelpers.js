const AlarmApp = artifacts.require("./AlarmApp.sol");
exports.accounts = []
exports.instance = {}
exports.result = {}

exports.getAmountInWei = (amount=1) => {
  return web3.toWei(amount, "ether")
}

////////////////////////////////////////////
// Ownable
////////////////////////////////////////////

// Owner
exports.ownerHasGetFunction = async () => {
  const owner = await this.instance.owner.call()
  assert.equal(owner, this.accounts[0], "can't get owner")
}

exports.ownerWithdrawWithdrawsCorrectAmount = async () => {
  const originalContractBalance = String(await web3.eth.getBalance(
                                         this.instance.address));
  const originalOwnerBalance = String(await web3.eth.getBalance(
                                         this.accounts[0]));
  const amount = this.getAmountInWei(0.5)

  // withdraw
  await this.instance.ownerWithdraw(amount, { from: this.accounts[0] })

  const finalContractBalance = String(await web3.eth.getBalance(
                                         this.instance.address));
  const finalOwnerBalance = String(await web3.eth.getBalance(
                                         this.accounts[0]));
  const difference = finalOwnerBalance - originalOwnerBalance
  const withdrawBalanceSuccess = difference > 497005600000000000
  /*console.log("original contract balance")
  console.log(originalContractBalance)
  console.log("original account balance")
  console.log(originalOwnerBalance)
  console.log("final contract balance")
  console.log(finalContractBalance)
  console.log("final account balance")
  console.log(finalOwnerBalance)*/
  assert.equal(originalContractBalance, this.getAmountInWei(1),
    "original contract balance not correct")
  assert.equal(finalContractBalance, this.getAmountInWei(0.5),
    "final contract balance not correct")
  assert.equal(withdrawBalanceSuccess, true,
    "owner didn't receive correct amount")
}

exports.ownerWithdrawRevertsIfNotOwner = async () => {
  let result = "success"
  const amount = this.getAmountInWei()
  try {
    await this.instance.ownerWithdraw(amount, { from: this.accounts[1] })
  } catch(error) {
    result = "revert"
  }
  assert.equal(result, "revert", "function call didn't revert")
}

////////////////////////////////////////////
// AlarmApp
////////////////////////////////////////////

// makeDeposit
exports.callMakeDeposit = async (options={}) => {
  let amount = options["amount"] || 1
  if (options["amount_zero"]) { amount = 0 }
  const txnOptions = {
    from: this.accounts[0],
    value: this.getAmountInWei(amount)
  }
  this.result = await this.instance.makeDeposit(txnOptions)
}

exports.makeDepositIsPublic = async () => {
  await this.callMakeDeposit()
  assert.equal(this.result["receipt"]["status"], 1, "function is not public")
}

exports.makeDepositIsPayable = async () => {
  const currentBalance = String(await web3.eth.getBalance(this.instance.address))
  const expectedBalance = String(this.getAmountInWei(1))
  assert.equal(currentBalance, expectedBalance, "incorrect deposit amount")
}

exports.makeDepositRevertsWhenZero = async () => {
  let result = "success"
  try {
    await this.callMakeDeposit({"amount_zero": true})
  } catch(error) {
    result = "revert"
  }
  assert.equal(result, "revert", "function call didn't revert")
}

exports.makeDepositEmitsEvent = async () => {
  const log = this.result["logs"][0]
  const args = log["args"]
  const amount = String(args["_amount"])
  assert.equal(log["event"], "DepositMade", "event name wrong")
  assert.equal(args["_accountAddressIndex"], this.accounts[0],
               "_accountAddressIndex wrong")
  assert.equal(args["_accountAddressIndex"], this.accounts[0],
               "_accountAddress wrong")
  assert.equal(amount, this.getAmountInWei(1), "_amount not correct")
}

////////////////////////////////////////////
// Killable
////////////////////////////////////////////

//killIReallyWa1ntToDoThis()
exports.killRevertsIfNotOwner = async () => {
  let result = "success"
  try {
    await this.instance.killIReallyWa1ntToDoThis({from: this.accounts[1]})
  } catch(error) {
    result = "revert"
  }
  assert.equal(result, "revert", "function call didn't revert")

}

exports.killRemovesContractAndSendsFundsToOwner = async () => {
  // test original contract balance
  const originalContractBalance = String(await web3.eth.getBalance(
                                         this.instance.address));
  let expectedOriginalContractBalance = String(this.getAmountInWei(1))
  assert.equal(originalContractBalance, expectedOriginalContractBalance,
               "original contract balance not correct")

  // get original account balance
  const originalAccountBalance = String(await web3.eth.getBalance(
                                        this.accounts[0]));
  // uncomment to see balances
  /*console.log("original contract balance")
  console.log(originalContractBalance)
  console.log("original account balance")
  console.log(originalAccountBalance)
  */

  // kill contract
  await this.instance.killIReallyWa1ntToDoThis({from: this.accounts[0]})

  /*
   * trying to test if code gets removed but below is not passing??
  this.instance = await AlarmApp.deployed()

  // test contract call reverts
  let result = "success"
  try {
    await this.callMakeDeposit()
    console.log(this.result)
  } catch(error) {
    result = "revert"
  }
  assert.equal(result, "revert", "killed contract function call didn't revert")
  */

  // NOTE: calling make deposit still deposits money into contract??
  // documentation says selfdestruct removes code???
  // await this.callMakeDeposit()

  // test owner balance after kill
  const newAccountBalance = String(await web3.eth.getBalance(this.accounts[0]))
  // account balance differences (now and before) should almost equal contract
  // original ammount
  const newAccountBalanceIsGood = (parseFloat(newAccountBalance) -
                                   parseFloat(originalAccountBalance) >
                                   998651199999995900);
  // test contract balance after kill
  const newContractBalance = String(await web3.eth.getBalance(
                                    this.instance.address));
  // uncomment to see balances
  /*console.log("new contract balance")
  console.log(newContractBalance)
  console.log("new account balance")
  console.log(newAccountBalance)
  console.log("account balance difference difference")
  console.log(parseFloat(newAccountBalance) - parseFloat(originalAccountBalance))
  console.log((newAccountBalance - originalAccountBalance) > 998552199999995900)
  */

  assert.equal(newContractBalance, 0, "new contract balance not correct")

  assert.equal(newAccountBalanceIsGood, true, "new account balance not correct")

}
