var HDWalletProvider = require("truffle-hdwallet-provider");
var mnemonic = "mnemonic here";

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!

  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    },
    rinkeby: {
      host: "localhost",
      port: 8545,
      from: "0x4cbf06d9046a6a647de63d98623de9359b14ee71", // default address to use for any transaction Truffle makes during migrations
      network_id: 4,
      gas: 4612388 // Gas limit used for deploys
    },
    live: {
      provider: function() {
        return new HDWalletProvider(mnemonic, "https://mainnet.infura.io/token_here")
      },
      from: "0x83047ecdaf3c2dfb0e9850bdb3311d8d5cb20e39", // must be lowercase. default address to use for any transaction Truffle makes during migrations
      network_id: 1,
      gas: 7888982, // Gas limit used for deploys
      gasPrice: 20000000000
    }

  }
};
