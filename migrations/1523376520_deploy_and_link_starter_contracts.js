var Ownable = artifacts.require("./zeppelin/ownership/Ownable.sol");
var Killable = artifacts.require("./zeppelin/lifecycle/Killable.sol");
var AlarmApp = artifacts.require("./AlarmApp.sol");

module.exports = function(deployer) {
  // AlarmApp
  deployer.deploy(Ownable);
  deployer.link(Ownable, Killable);
  deployer.deploy(Killable);
  deployer.link(Killable, AlarmApp);
  deployer.deploy(AlarmApp);
};
