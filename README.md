# README #

### What is this repository for? ###

* This contains a truffle framework, react webapp, solidity contracts, web3
* App motivates user to keep punctual for appointments or alarms

### How do I get set up? ###

* run `npm install -g ganache-cli`
* run `npm install truffle`
* clone Repo
* create a `.env` file and copy content from `.env.example`
* update `.env` variable values accordingly
* run `npm install`
* run `ganache-cli`
* run `truffle migrate`
* run `npm start`
