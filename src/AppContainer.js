import { connect } from 'react-redux'
import App from './App'
import getAccountInfo from './models/account/actions/get'

const mapStateToProps = (state, ownProps) => {
  return {}
}

const mapDispatchToProps = (dispatch) => {
  return {
    getAccountInfo: (event) => {
      event.preventDefault();

      dispatch(getAccountInfo())
    }
  }
}

const AppContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(App)

export default AppContainer
