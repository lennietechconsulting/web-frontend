import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import styles from './styles.css'
import { withStyles  } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class MetamaskGuide extends Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <div id="metamaskGuideContainer">
        <Card>
          <CardContent>
            <br />
            <Typography variant="display2" gutterBottom align="center">
              Metamask
            </Typography>
            <div id="metamask-image">
              <a href="https://metamask.io/" target="_blank">
                <img id="metamask-image"src="https://f001.backblazeb2.com/file/cryptogoal-images/Screenshot+from+2018-07-07+23-15-58.png"/>
              </a>
            </div>
            <br />
            <Typography id="title-small" variant="body2" gutterBottom align="center">
              Metamask is used to connect your browser with Ethereum. You will need it in order to make deposits and create goals.
            </Typography>
            <Typography id="title-big" variant="body2" gutterBottom align="center">
              Metamask is used to connect your browser with Ethereum.
              <div></div>
              You will need it in order to make deposits and create goals.
            </Typography>
          </CardContent>
        </Card>
        <div id="metamask-desktop-and-mobile">
          <Card id="metamask-mobile">
            <CardContent>
              <br />
              <Typography variant="title" gutterBottom align="center">
                Mobile Phone
              </Typography>
              <br />
              <Typography variant="body2" gutterBottom align="center">
                Must use <a href="https://www.mozilla.org" target="_blank">Firefox</a> with <a href="https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/" target="_blank">Metamask</a> Addon
              </Typography>
            </CardContent>
          </Card>
          <Card id="metamask-desktop">
            <CardContent>
              <br />
              <Typography variant="title" gutterBottom align="center">
                Desktop or Laptop
              </Typography>
              <br />
              <Typography variant="body2" gutterBottom align="center">
                Use <a href="https://www.mozilla.org" target="_blank">Firefox</a> or <a href="https://www.google.com/chrome" target="_blank">Chrome</a>. Then install <a href="https://metamask.io/" target="_blank">Metamask</a>

              </Typography>
            </CardContent>
          </Card>
        </div>
        <br />
        {this.props.onboarding &&
          <div>
            <br />
            <div id="center-button">
              <Button color="primary" variant="outlined" onClick={this.props.skip}>
                I have installed and logged into metamask. Let's make some Goals!
              </Button>
            </div>
          </div>
        }
      </div>
    )
  }
}

export default withStyles(styles)(MetamaskGuide);
