import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import styles from './styles.css'

class Contact extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <div id="padLayout">
        <Typography id="balanceAmount" variant="display1" gutterBottom align="center">
          Contact Us
        </Typography>
        <Typography id="balanceAmount" variant="body2" gutterBottom align="center">
          We would love to hear from you.
          <div></div>
          Please use the following email for all communication.
        </Typography>
        <Typography id="balanceAmount" variant="title" gutterBottom align="center">
          hello@cryptogoals.io
        </Typography>
      </div>
    )
  }
}

export default withStyles(styles)(Contact);
