import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import { Link } from 'react-router'
import Typography from '@material-ui/core/Typography'
import styles from './styles.css'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Title from '../../ui/about/title/component'
import Description from '../../ui/about/description/component'
import Instructions from '../../ui/about/instructions/component'

class About extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    // title
    // tagline
    // description
    // how to use
    // vision
    return(
      <div>
        <Title skip={this.props.skip} onboarding={this.props.onboarding} />
        <Description />
        <Instructions skip={this.props.skip} onboarding={this.props.onboarding} />
      </div>
    )
  }
}

export default withStyles(styles)(About);

