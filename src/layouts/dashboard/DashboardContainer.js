import { connect } from 'react-redux'
import Dashboard from './Dashboard'

const mapStateToProps = (state, ownProps) => {
  return {
    address: state.account.address,
    balance: state.account.balance,
    usableBalance: state.account.usableBalance,
    web3: state.web3
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

const DashboardContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard)

export default DashboardContainer
