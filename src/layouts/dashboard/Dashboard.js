import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import { Link } from 'react-router'
import GoalListSection from '../../models/goal/components/list-section/container'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import styles from './dashboard.css'
import withWeb3Check from '../../ui/wrappers/withWeb3Check'
import withOnboarding from '../../ui/wrappers/withOnboarding'

class Dashboard extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let web3Instance = this.props.web3.web3Instance
    let balance
    if (web3Instance) {
      balance = web3Instance.fromWei(this.props.balance, "ether");
    }
    return(
      <div id="padLayout">
        <Typography id="balanceAmount" variant="body1" gutterBottom align="center">
          Balance: { balance || "loading ..." } Ether
        </Typography>
        <div id="navButtons">
          <Button component={Link} to="/deposit" variant="outlined" color="primary">
            Make Deposit
          </Button>
          <Button component={Link} to="/new-goal" variant="outlined" color="primary">
            Add New Goal
          </Button>
        </div>
      <GoalListSection />
      </div>
    )
  }
}

export default withOnboarding(withWeb3Check(withStyles(styles)(Dashboard)));
