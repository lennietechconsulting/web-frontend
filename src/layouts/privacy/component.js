import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import styles from './styles.css'
import { withStyles  } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Privacy extends Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <div id="privacyContainer">
        <Typography variant="display1" gutterBottom align="center">
          Privacy Policy
        </Typography>
        <Typography variant="title" gutterBottom>
          Overview
        </Typography>
        <Typography gutterBottom>
        This privacy notice discloses the privacy practices for https://cryptogoals.io This privacy notice applies solely to information collected by this website. It will notify you of the following:
        </Typography>
        <ol>
          <li>What personally identifiable information is collected from you through the website, how it is used and with whom it may be shared.</li>
          <li>What choices are available to you regarding the use of your data.</li>
          <li>The security procedures in place to protect the misuse of your information.</li>
          <li>How you can correct any inaccuracies in the information.</li>
        </ol>
        <Typography variant="title" gutterBottom>
          Information Collection, Use, and Sharing
        </Typography>
        <Typography gutterBottom>
          We are the sole owners of the information collected on this site. We only have access to/collect information that you voluntarily give us via forms, email, and interaction via Metamask and Ethereum. We will not sell or rent this information to anyone.
          We will use your information to respond to you, regarding the reason you contacted us. We will not share your information with any third party outside of our organization, other than as necessary to fulfill your request, e.g. complete transaction with Ethereum.
        </Typography>
        <Typography variant="title" gutterBottom>
          Security
        </Typography>
        <Typography gutterBottom>
          We take precautions to protect your information. When you submit sensitive information via the website, your information is protected both online and offline.
      Wherever we collect sensitive information, that information is encrypted and transmitted to us in a secure way. You can verify this by looking for a lock icon in the address bar and looking for "https" at the beginning of the address of the Web page.
      While we use encryption to protect sensitive information transmitted online, we also protect your information offline. Only employees who need the information to perform a specific job are granted access to personally identifiable information. The computers/servers in which we store personally identifiable information are kept in a secure environment.
        </Typography>
        <Typography variant="title" gutterBottom>
          What We Store
        </Typography>
        <Typography gutterBottom>
          We store information in our database and we store information in your browser via local storage and cookies. Below is an explanation of what we store and why.
        </Typography>
        <Typography variant="title" gutterBottom>
          Ethereum Account Public Address and Transaction Information
        </Typography>
        <Typography gutterBottom>
          If you make a deposit or transaction via Metamask, we store your Ethereum account public address in our database.

          We also store transaction information made when you deposit ether via metamask.
          This transaction information includes transaction hash, block number, transaction status.

          We store this information so that you can deposit funds and then use those funds to create goals. Storing this information allows us to keep track of how much you have deposited into the app.
        </Typography>
        <Typography gutterBottom>
          CryptoGoals never sees nor saves your Ethereum account private information
      We use Metamask as an intermediary to handle all Ethereum transactions. We will never see nor save your ethereum private information such as a private key, seed phrase or password. We do however use your Ethereum account public address to keep track of who you are, and we associate that address with any activity that account has performed with CryptoGoals such as how much you have deposited and any goals you have created.
        </Typography>
        <Typography variant="title" gutterBottom>
          Location Tracking
        </Typography>
        <Typography gutterBottom>
          We ask permission to view your location. This is to help us find your next goal location and it allows us to know if you have reached your goal in time or not. When creating a new goal, we store the goal location in our database. That is the only location or address that we store in our database.
        </Typography>
        <Typography variant="title" gutterBottom>
          Links
        </Typography>
        <Typography gutterBottom>
          This website contains links to other sites. Please be aware that we are not responsible for the content or privacy practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy statements of any other site that collects personally identifiable information.
        </Typography>
        <Typography variant="title" gutterBottom>
          Cookies and Local Storage
        </Typography>
        <Typography gutterBottom>
          Please view the <a href="/cookies">Cookies Page</a> for more information.
        </Typography>
        <Typography variant="title" gutterBottom>
          Contact
        </Typography>
        <Typography gutterBottom>
          If you feel that we are not abiding by this privacy policy or you have any questions, you should contact us immediately via email at hello@cryptogoals.io
        </Typography>
      </div>
    )
  }
}

export default withStyles(styles)(Privacy);
