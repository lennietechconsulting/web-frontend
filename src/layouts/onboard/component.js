import React, { Component } from 'react'
import About from '../about/component'
import MetamaskGuide from '../metamaskGuide/component'
import styles from './styles.css'
import { withStyles  } from '@material-ui/core/styles';
import { browserHistory } from 'react-router'

class Onboard extends Component {
  constructor(props) {
    super(props)

    this.state = {
      aboutAccepted: false,
      metamaskAccepted: false
    }
  }

  checkForOnboarding() {
    let aboutAccepted = false
    let metamaskAccepted = false

    if (window.localStorage.getItem("completedOnboarding") === "true") {
      return window.location = "/"
    }

    if (window.localStorage.getItem("aboutAccepted") === "true") {
      aboutAccepted = true
    }

    if (window.localStorage.getItem("metamaskAccepted") === "true") {
      metamaskAccepted = true
    }

    if (aboutAccepted && metamaskAccepted) {
      window.localStorage.setItem("completedOnboarding", "true")
      return window.location = "/"
    }

    this.setState({
      aboutAccepted: aboutAccepted,
      metamaskAccepted: metamaskAccepted
    })

  }

  componentDidMount() {
    this.checkForOnboarding()
  }

  acceptAbout() {
    window.localStorage.setItem("aboutAccepted", "true")
    this.checkForOnboarding()
  }

  acceptMetamask() {
    window.localStorage.setItem("metamaskAccepted", "true")
    this.checkForOnboarding()
  }

  render() {
    return (
      <div>
        {!this.state.aboutAccepted &&
          <About onboarding={true}
                 skip={this.acceptAbout.bind(this)}/>
        }
        {this.state.aboutAccepted &&
          <MetamaskGuide onboarding={true}
                 skip={this.acceptMetamask.bind(this)}/>
        }
      </div>
    )
  }
}

export default withStyles(styles)(Onboard);

