import React, { Component } from 'react'
import { Link } from 'react-router'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button'
import DialogActions from '@material-ui/core/DialogActions';
import AppBarComponent from '../../../ui/appBarComponent/appBarComponent';
import Terms from '../component'

class TermsDialog extends Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {

    return(
      <div>
        <Dialog
          fullScreen={true}
          open={this.props.open}
          onClose={this.props.closeDialog}
          aria-labelledby="terms-dialog-title"
          aria-describedby="terms-dialog-description"
        >
          <AppBarComponent />
          <DialogActions>
            <Button onClick={this.props.closeDialog} color="primary">
              Close
            </Button>
          </DialogActions>
          <Terms />
          <DialogActions>
            <Button onClick={this.props.closeDialog} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default TermsDialog;
