import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import styles from './styles.css'
import { withStyles  } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Terms extends Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <div id="termsContainer">
        <Typography variant="display1" gutterBottom align="center">
          Terms and Conditions
        </Typography>
        <Typography variant="title" gutterBottom>
          The App and the Company
        </Typography>
        <Typography gutterBottom>
          CryptoGoals will be henceforth known as “the App”.
          The App is owned and operated by Lennie Tech Consulting Inc, henceforth known as “the Company”.
        </Typography>
        <Typography gutterBottom>
          You, the user of the App, confirm your acceptance of these App terms of use (“App Terms”). If you do not agree to these App Terms, you must immediately uninstall the App and discontinue its use.  These App Terms should be read alongside our <a href="/privacy">Privacy Policy</a> and <a href="/cookies">Cookie Policy.</a>
        </Typography>
        <Typography variant="title" gutterBottom>
          The App and the Company
        </Typography>
        <Typography variant="title" gutterBottom>
          App and Related Terms
        </Typography>
        <Typography gutterBottom>
          We may from time to time vary these App Terms. Please check these App Terms regularly to ensure you are aware of any variations made by us. If you continue to use this App, you are deemed to have accepted such variations. If you do not agree to such variations, you should not use the App.
        </Typography>
        <Typography variant="title" gutterBottom>
          Use of the App
        </Typography>
        <Typography gutterBottom>
          The Company hereby grants you a non-exclusive, non-transferable, revocable licence to use the App for your personal, non-commercial use. All rights in the App are reserved by the Company.
      In the event of your breach of these App Terms we will be entitled to terminate the User Licence immediately.
      You acknowledge that your agreement with your mobile network provider (“Mobile Provider”) will apply to your use of the App. You acknowledge that you may be charged by the Mobile Provider for data services while using certain features of the App or any such third party charges as may arise and you accept responsibility for such charges. If you are not the bill payer for the Device being used to access the App, you will be assumed to have received permission from the bill payer for using the App.
      You acknowledge that where you use services provided by Apple or Google (or any other third parties) in connection with your use of the App, you will be subject to Apple’s, Google’s (or the applicable third party’s) terms and conditions and privacy policy and you should ensure that you have read such terms.
        </Typography>
        <Typography variant="title" gutterBottom>
          Prohibited Uses
        </Typography>
        <Typography gutterBottom>
          You agree not to use the App in any way that:
        </Typography>
        <ul>
          <li>is unlawful, illegal or unauthorised;</li>
          <li>is defamatory of any other person;</li>
          <li>is obscene or offensive;</li>
          <li>promotes discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</li>
          <li>infringes any copyright, database right or trade mark of any other person;</li>
          <li>is likely to harass, upset, embarrass, alarm or annoy any other person;</li>
          <li>is likely to disrupt our service in any way; or</li>
          <li>advocates, promotes or assists any unlawful act such as (by way of example only) copyright infringement or computer misuse.</li>
        </ul>
        <Typography variant="title" gutterBottom>
          Intellectual Property and Ownership
        </Typography>
        <Typography gutterBottom>
          CryptoGoals is owned and operated by Lennie Tech Consulting Inc., henceforth known as the “Company”, which is a British Columbia, Canadian, Corporation. 
The Company’s name and logo, and other Company trademarks, service marks, graphics and logos used in connection with the App are trademarks of the company.  Other trademarks, service marks, graphics and logos used in connection with the App are the trademarks of their respective owners (collectively “Third Party Trademarks”).  The Company’s Trademarks and Third Party Trademarks may not be copied, imitated or used, in whole or in part, without the prior written permission of the company or the applicable trademark holder. The App and the content featured in the App are protected by copyright, trademark, patent and other intellectual property and proprietary rights which are reserved to the Company and its licensors.
        </Typography>
        <Typography variant="title" gutterBottom>
          Indemnification
        </Typography>
        <Typography gutterBottom>
          You agree to indemnify The Company for any breach of these App Terms. The Company reserves the right to control the defence and settlement of any third party claim for which you indemnify the Company under these App Terms and you will assist us in exercising such rights.
        </Typography>
        <Typography variant="title" gutterBottom>
          No Promises
        </Typography>
        <Typography gutterBottom>
          The Company provides the App on an ‘as is’ and ‘as available’ basis without any promises or representations, express or implied. In particular, the Company does not warrant or make any representation regarding the validity, accuracy, reliability or availability of the App or its content. To the fullest extent permitted by applicable law, the Company hereby excludes all promises, whether express or implied, including any promises that the App is fit for purpose, of satisfactory quality, non-infringing, is free of defects, is able to operate on an uninterrupted basis, that the use of the App by you is in compliance with laws or that any information that you transmit in connection with this App will be successfully, accurately or securely transmitted.
        </Typography>
        <Typography variant="title" gutterBottom>
          We take no responsibility for any issues that arise from using our product
        </Typography>
        <Typography gutterBottom>
          Although we want to build a high quality and trustworthy product, this is an experimental application using relatively new and experimental technology such as Ethereum, blockchain and related technologies. Because of this, CryptoGoals takes no responsibility when using the CryptoGoals application.

Issues that we do not take responsibility for are transaction errors when using Metamask or ethereum related issues such as misplacing private keys, seed phrases or passwords. Always make sure your credentials are never revealed to the public and safely secured.
        </Typography>
        <Typography variant="title" gutterBottom>
          Currently, we do not offer withdrawals or refunds
        </Typography>
        <Typography gutterBottom>
          In the future, we plan to allow users to be rewarded and to allow users to withdraw their funds. Currently this feature is not implemented and currently we do not allow for withdrawals or refunds.
        </Typography>
        <Typography variant="title" gutterBottom>
          Exclusion of the Company’s Liability
        </Typography>
        <Typography gutterBottom>
          Nothing in these App Terms shall exclude or in any way limit the Company’s liability for death or personal injury caused by its negligence or for fraud or any other liability to the extent the same may not be excluded or limited as a matter of law.
To the fullest extent permitted under applicable law, in no event shall the Company be liable to you with respect to use of the App and/or be liable to you for any direct, indirect, special or consequential damages including, without limitation, damages for loss of goodwill, lost profits, or loss, theft or corruption of your information, the inability to use the App, Device failure or malfunction.
The Company shall not be liable even if it has been advised of the possibility of such damages, including without limitation damages caused by error, omission, interruption, defect, failure of performance, unauthorised use, delay in operation or transmission, line failure, computer virus, worm, Trojan horse or other harm. 
        </Typography>
        <Typography variant="title" gutterBottom>
          General
        </Typography>
        <Typography gutterBottom>
          These App Terms shall be governed by the laws of British Columbia Canada and the parties submit to the exclusive jurisdiction of the courts of Canada to resolve any dispute between them arising under or in connection with these App Terms.
If any provision (or part of a provision) of these App Terms is found by any court or administrative body of competent jurisdiction to be invalid, unenforceable or illegal, such term, condition or provision will to that extent be severed from the remaining terms, conditions and provisions which will continue to be valid to the fullest extent permitted by law.
        </Typography>
        <Typography variant="title" gutterBottom>
          Contact Information
        </Typography>
        <Typography gutterBottom>
          You are free to email us at hello@cryptogoals.io
        </Typography>
      </div>
    )
  }
}

export default withStyles(styles)(Terms);
