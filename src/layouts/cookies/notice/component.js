import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import styles from "./styles.css"
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import InfoIcon from '@material-ui/icons/Info';
import IconButton from '@material-ui/core/IconButton';

class CookieNotice extends Component {

  constructor(props) {
    super(props)

    this.state = {
      show: false
    }
  }

  handleClose() {
    window.localStorage.setItem("cookiesAccepted", "true")
    this.setState({show: false})
  }

  checkIfAcceptedCookies() {
    if (window.location.pathname == "/cookies") {
      return
    }

    if (window.localStorage.getItem("cookiesAccepted") === "true") {
      return
    } else {
      this.setState({show: true})
    }
  }

  componentDidMount() {
    this.checkIfAcceptedCookies()
  }

  render() {
    return (
      <Snackbar
        id="cookieNoticeContainer"
        fullWidth={true}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={this.state.show}
        onClose={this.handleClose.bind(this)}
      >
        <SnackbarContent
          message={
            <span id="client-snackbar">
              <InfoIcon size="small" />
              We use cookies to provide you with the best possible experience. You can view our Cookie Policy <a href="/cookies" target="_blank">here.</a> By clicking "continue" or continuing to use our site you acknowledge that you accept this policy.
            </span>
          }
          action={[
            <IconButton
              key="close"
              aria-label="Close"
              size="small"
              color="inherit"
              onClick={this.handleClose.bind(this)}
            >
              Continue
            </IconButton>,
          ]}
        />
      </Snackbar>
    );
  }
}

export default withStyles(styles)(CookieNotice);
