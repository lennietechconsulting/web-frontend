import React, { Component } from 'react'
import Button from '@material-ui/core/Button'
import styles from './styles.css'
import { withStyles  } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography'
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Cookies extends Component {
  constructor(props) {
    super(props)

    this.state = {
    }
  }

  render() {
    return (
      <div id="cookiesContainer">
        <Typography variant="display1" gutterBottom align="center">
          Cookies Policy
        </Typography>
        <Typography gutterBottom>
          To make this site work properly, we sometimes place small data files called cookies on your device. Most big websites do this too.
        </Typography>
        <Typography variant="title" gutterBottom>
          What Are Cookies And What Is Local Storage?
        </Typography>
        <Typography gutterBottom>
          A cookie is a small text file that a website saves on your computer or mobile device when you visit the site. It enables the website to remember your actions and preferences (such as login, language, font size and other display preferences) over a period of time, so you don’t have to keep re-entering them whenever you come back to the site or browse from one page to another.
          Usage of a cookie is in no way linked to any personally identifiable information on our site.
          Some of our business partners may use cookies on our site, for example Google analytics. However, we have no access to or control over these cookies.
        </Typography>
        <Typography gutterBottom>
          Enabling these cookies is not strictly necessary for the website to work but it will provide you with a better browsing experience. You can delete or block these cookies, but if you do that some features of this site may not work as intended.
      The cookie-related information is not used to identify you personally and the pattern data is fully under our control. These cookies are not used for any purpose other than those described here.
        </Typography>
        <Typography gutterBottom>
          Local Storage is very similar to a cookie except that data is saved as a key/value pair instead of a text file.
        </Typography>
        <Typography gutterBottom>
          You can find more information on localStorage here: https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
          And more information about cookies here:
          http://www.allaboutcookies.org/cookies/
        </Typography>
        <Typography variant="title" gutterBottom>
          Accepting Cookie policy
        </Typography>
        <Typography gutterBottom>
          When you first visit the App, you may see a banner asking you to accept the cookie policy. If you agree, we will store in local storage in your browser that you have accepted the cookie policy. We do this so that the next time you visit the site, you will not have to accept the cookie policy again.
        </Typography>
        <Typography variant="title" gutterBottom>
          Onboarding Introduction
        </Typography>
        <Typography gutterBottom>
          We use local storage to store content in your browser so that we know if you have completed the onboarding introduction or not. When you first go to the app, there will be an introduction. Once you click on a button to go to the next page, we store that you have completed the introduction in local storage so that on next page load, you will not have to see the introduction again. We do this to make your experience more friendly.
        </Typography>
        <Typography variant="title" gutterBottom>
          Google Analytics
        </Typography>
        <Typography gutterBottom>
          We use google analytics to track page views and user activity throughout the app. Google analytics stores information in your browser via cookies
        </Typography>
        <Typography variant="title" gutterBottom>
          Location Tracking
        </Typography>
        <Typography gutterBottom>
          We ask permission to view your location. This is to help us find your next goal location and it allows us to know if you have reached your goal in time or not. When creating a new goal, we store the goal location in our database. That is the only location or address that we store in our database. Cookies are also stored to remember if you have already given location permission or not.
        </Typography>
        <Typography variant="title" gutterBottom>
          How To Control Cookies
        </Typography>
        <Typography gutterBottom>
          You can control and/or delete cookies as you wish – for details, see aboutcookies.org. You can delete all cookies that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some services and functionalities may not work.
        </Typography>
        <Typography variant="title" gutterBottom>
          How To Control Local Storage
        </Typography>
        <Typography gutterBottom>
          You can control and/or delete local storage keys and values as you wish – for details, see https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage. You can delete all local storage keys and values that are already on your computer and you can set most browsers to prevent them from being placed. If you do this, however, you may have to manually adjust some preferences every time you visit a site and some services and functionalities may not work.
        </Typography>
        <Typography variant="title" gutterBottom>
          Contact
        </Typography>
        <Typography gutterBottom>
          If you feel that we are not abiding by this cookie policy or you have any questions, you should contact us immediately via email at hello@cryptogoals.io
        </Typography>
      </div>
    )
  }
}

export default withStyles(styles)(Cookies);
