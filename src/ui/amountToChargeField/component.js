import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Typography from '@material-ui/core/Typography'
import styles from './amountToCharge.css'

class AmountToChargeField extends Component {
  constructor(props) {
    super(props)

    this.state = {
      amount: ""
    }
  }

  handleAmountChange = e => {
    const value = parseFloat(e.target.value)
    this.setState({ amount: value });
    this.props.updateFormInfo({ amount: value });
  }

  render() {
    // then update parent component
    return(
      <FormControl id="amountToChargeContainer" className="form-address-field">
        <InputLabel htmlFor="form-address-field">Ether Amount</InputLabel>
        <Input type="number" id="form-address-field" value={this.state.amount}
                                onChange={this.handleAmountChange} />
        <Typography variant="caption" gutterBottom>
          * This is the amount that will be charged to your account if you do not complete your goal by the deadline given.
        </Typography>
      </FormControl>
    )
  }
}

export default withStyles(styles)(AmountToChargeField);
