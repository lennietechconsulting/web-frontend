import React, { Component } from 'react'
import Button from '@material-ui/core/Button'

class Geolocation extends Component {
  constructor(props) {
    super(props)

    this.state = {
      askForGeolocation: false,
      showGeolocationDeniedMessage: false,
      currentLatitude: null,
      currentLongitude: null
    }
  }

  componentDidMount() {
    this.checkLocation()
  }

  updateAllStates(newData) {
    this.setState(newData)
    this.props.updateFormInfo(newData)
  }

  geoSuccess(response) {
    const newData = {
      askForGeolocation: false,
      showGeolocationDeniedMessage: false,
      currentLatitude: response.coords.latitude,
      currentLongitude: response.coords.longitude,
    }
    this.updateAllStates(newData)
  }

  geoError(err) {
    if (err.message === "User denied Geolocation") {
      const newData = {
        askForGeolocation: false,
        showGeolocationDeniedMessage: false,
      }
      this.updateAllStates(newData)
      this.checkLocation()
    }
  }

  getLocation() {
    const _this = this
    return new Promise((resolve, reject) => {
      try {
      	navigator.geolocation.getCurrentPosition(
      	(response) => {
        	_this.geoSuccess(response)
        	resolve()
      	},
      	(err) => {
        	_this.geoError(err);
        	reject(err)
      	})
      } catch(error) {
        let errorString = JSON.stringify(error, null, 4)
        reject(error)
      }
    })
  }

  checkLocation() {
    navigator.permissions.query({'name': 'geolocation'})
      .then( permission => {
        if (permission.state === "granted") { this.getLocation() }
        if (permission.state === "prompt") {
          const newData = { askForGeolocation: true }
          this.updateAllStates(newData)
        }
        if (permission.state === "denied") {
          const newData = { showGeolocationDeniedMessage: true }
          this.updateAllStates(newData)
        }
      })
  }

  render() {
    return(
      <div>
        { this.state.askForGeolocation &&
          <div>
            <p>In order to complete your goal, we will need your location.</p>
            <Button color="primary" variant="outlined" onClick={this.getLocation.bind(this)}>
              Click here to allow us to view your location
            </Button>
          </div>
        }
        { this.state.showGeolocationDeniedMessage &&
          <div>
            <p>It looks like you have denied access to your location.</p>
            <p>Please fix this and then refresh page</p>
            <p>Links on how to fix</p>
            <p><a href="https://superuser.com/questions/591758/how-do-i-make-chrome-forget-a-no-to-geolocation-on-a-site" target="_blank">Chrome</a></p>
            <p><a href="https://apple.stackexchange.com/questions/122930/how-do-i-permanently-allow-a-website-to-access-my-location" target="_blank">Safari</a></p>
            <p><a href="https://support.mozilla.org/en-US/kb/does-firefox-share-my-location-websites" target="_blank">Firefox</a></p>
            <div>
            </div>
          </div>
        }
      </div>
    )
  }
}

export default Geolocation
