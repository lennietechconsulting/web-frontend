import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import scriptLoader from 'react-async-script-loader'
import styles from './addressField.css'
import Input from '@material-ui/core/Input'
import FormControl from '@material-ui/core/FormControl'
import Typography from '@material-ui/core/Typography'

// Expects an updateAddress() prop
// Expects currentLatitude and currentLongitude prop
class AddressField extends Component {
  constructor(props) {
    super(props)
    this.google = null
  }

  componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed }) {
    if (isScriptLoaded && !this.props.isScriptLoaded) { // load finished
      if (isScriptLoadSucceed) {
        this.initAutocomplete()
      }
    }
  }

  // TODO add search button.
  // Start from user's current location and bound
  initAutocomplete() {

    const _this = this

    var map = new window.google.maps.Map(document.getElementById('map'), {
      center: {lat: this.props.currentLatitude,
               lng: this.props.currentLongitude},
      zoom: 13,
      disableDefaultUI: true
    });


    const input = document.getElementById('pac-input');
    const autocomplete = new window.google.maps.places.Autocomplete(input);
    autocomplete.bindTo('bounds', map);

  	const infowindow = new window.google.maps.InfoWindow();
  	const infowindowContent = document.getElementById('infowindow-content');
  	infowindow.setContent(infowindowContent);
  	const marker = new window.google.maps.Marker({
    	map: map,
    	anchorPoint: new window.google.maps.Point(0, -29)
  	});

    autocomplete.addListener('place_changed', function() {
      infowindow.close();
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        window.alert("No details available for input: '" + place.name + "'");
        return;
      }

      const formatted_address_info = {
        formattedAddress: place.formatted_address,
        latitude: place.geometry.location.lat(),
        longitude: place.geometry.location.lng(),
      }

      _this.props.updateFormInfo(formatted_address_info)

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      infowindowContent.children['place-icon'].src = place.icon;
      infowindowContent.children['place-name'].textContent = place.name;
      infowindowContent.children['place-address'].textContent = address;
      infowindow.open(map, marker);
    });

  }

  render() {
    return(
      <div>
        <FormControl id="pac-container" className="form-field">
          <Input id="pac-input" placeholder="Enter Location to Reach Here"/>
        </FormControl>
        <Typography variant="caption" gutterBottom>
          * In case of slow internet connection, please be patient with address autocomplete field.
        </Typography>

        <div id="map"></div>
        <div id="infowindow-content">
      		<img src="" width="16" height="16" id="place-icon"></img>
      		<span id="place-name" className="title"></span><br/>
      		<span id="place-address"></span>
    		</div>
      </div>
    )
  }
}

export default scriptLoader(
  [
    'https://maps.googleapis.com/maps/api/js?key=AIzaSyDxWtCQP3qDWrEMNZQJlPj_wK-KqDVVU74&libraries=places'
  ]
)(withStyles(styles)(AddressField))
