import React from 'react';
import PropTypes from 'prop-types';
import { withStyles  } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SideMenu from '../sideMenu/component';

const styles = {
  root: {
  },
  flex: {
    flex: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },

};

class AppBarComponent extends React.Component {

  state = {
    open: false
  };

  openSideMenu() {
    this.setState({open: true})
  }

  closeSideMenu() {
    this.setState({open: false})
  }

  render() {
    const { classes  } = this.props;
    return (
      <div className={classes.root}>
        <SideMenu open={this.state.open}
                  close={this.closeSideMenu.bind(this)} />
        <AppBar position="static">
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open side menu"
              onClick={this.openSideMenu.bind(this)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="title" color="inherit" className={classes.flex}>
              CryptoGoals
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }

}

AppBarComponent.propTypes = {
    classes: PropTypes.object.isRequired,

};

export default withStyles(styles)(AppBarComponent);
