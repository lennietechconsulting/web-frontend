import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Divider from '@material-ui/core/Divider';
import styles from './styles.css'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';

class SideMenu extends React.Component {

  render() {
    const { classes } = this.props;

    return (
      <Drawer open={this.props.open} onClose={this.props.close}>
        <List id="side-menu-list" component="nav">
          <ListItem button onClick={this.props.close} divider={true}>
            <ListItemIcon>
              <ChevronLeftIcon />
            </ListItemIcon>
          </ListItem>
          <ListItem button component="a" href="/" divider={true}>
            <ListItemText>Home</ListItemText>
          </ListItem>
          <ListItem button component="a" href="/about" divider={true}>
            <ListItemText>About</ListItemText>
          </ListItem>
          <ListItem button component="a" href="/faqs" divider={true}>
            <ListItemText>FAQs</ListItemText>
          </ListItem>
          <ListItem button component="a" href="/metamask-guide" divider={true}>
            <ListItemText>Metamask</ListItemText>
          </ListItem>
          <ListItem button component="a" href="/contact" divider={true}>
            <ListItemText>Contact</ListItemText>
          </ListItem>
          <ListItem button component="a" href="/terms" divider={true}>
            <ListItemText>Terms</ListItemText>
          </ListItem>
          <ListItem button component="a" href="/privacy" divider={true}>
            <ListItemText>Privacy</ListItemText>
          </ListItem>
          <ListItem button component="a" href="/cookies" divider={true}>
            <ListItemText>Cookies</ListItemText>
          </ListItem>
        </List>
      </Drawer>
    );
  }
}

SideMenu.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SideMenu);
