import React, { Component } from 'react'
import { Link } from 'react-router'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button'

class Web3Error extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    let title = ""
    let text = ""
    let metamaskLink = <div></div>

    if (this.props.web3Error === "noWeb3") {
      title = "No Ethereum Connection Found"
      text = "Please make sure that you have installed and logged into "
      metamaskLink = <a href="https://metamask.io/" target="_blank">Metamask</a>

    } else if (this.props.web3Error === "noAccount") {
      title = "No Ethereum Account Found"
      text = "Please make sure that you have logged into Metamask."

    } else if (this.props.web3Error === "wrongNetwork") {
      title = "Wrong Network Detected"
      text = "Please make sure you are connected to the main ethereum network."
    }

    return(
      <div>
        <Dialog
          open={true}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle align="center" id="alert-dialog-title">{title}</DialogTitle>
          <DialogContent>
            <DialogContentText align="center" id="alert-dialog-description">
              {text}
              {metamaskLink}
            </DialogContentText>
            <div>
              <br />
              <Button component={Link}
                      fullWidth={true}
                      to="/metamask-guide"
                      color="primary"
                      variant="outlined">
                More Info Here
              </Button>
            </div>
          </DialogContent>
        </Dialog>
      </div>
    )
  }
}

export default Web3Error;
