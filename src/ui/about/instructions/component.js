import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import { Link } from 'react-router'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import styles from './styles.css'
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Instructions extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <Card id="about-instructions-section">
        <CardContent>
          <Typography variant="display1" gutterBottom align="center">
           How does CryptoGoals work?
          </Typography>
          <div id="instructions-content">
            <Card id="instruction-card">
              <CardContent>
                <br />
                <Typography variant="title" gutterBottom align="center">
                  Create Goals
                </Typography>
                <img id='instruction-img' src="https://f001.backblazeb2.com/file/cryptogoal-images/new-goal-page.png"/>
                <br />
              </CardContent>
            </Card>
            <Card id="instruction-card">
              <CardContent>
                <br />
                <Typography variant="title" gutterBottom align="center">
                  View Goals
                </Typography>
                <img id='instruction-img' src="https://f001.backblazeb2.com/file/cryptogoal-images/homepage.png"/>
                <br />
              </CardContent>
            </Card>
            <Card id="instruction-card">
              <CardContent>
                <br />
                <Typography variant="title" gutterBottom align="center">
                  Complete Goals
                </Typography>
                <div id="instruction-img-3-container">
                  <img id='instruction-img' src="https://f001.backblazeb2.com/file/cryptogoal-images/details-page.png"/>
                </div>
                <br />
              </CardContent>
            </Card>
          </div>
          {this.props.onboarding &&
            <div>
              <br />
              <div id="center-button">
                <Button color="primary" variant="outlined" onClick={this.props.skip}>
                  Start adding Goals!
                </Button>
              </div>
            </div>
          }
        </CardContent>
      </Card>
    )
  }
}

export default withStyles(styles)(Instructions);
