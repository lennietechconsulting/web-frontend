import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import { Link } from 'react-router'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import styles from './styles.css'
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Description extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <div id="about-description-section">
        <Card id="about-description-card">
          <CardContent>
            <Typography variant="headline" color="#fff" gutterBottom>
              What is CryptoGoals?
            </Typography>

            <Typography variant="body2" color="#fff" gutterBottom>
              CryptoGoals is a motivational Dapp that uses Ether to motivate users to stick to their goals. If goals are not completed then users are charged. In the future we are also planning to reward users in Ether for completing goals.
            </Typography>
          </CardContent>
        </Card>
      </div>
    )
  }
}

export default withStyles(styles)(Description);

