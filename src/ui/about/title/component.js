import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import { Link } from 'react-router'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import styles from './styles.css'
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

class Title extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    let text =  "Stay motivated and stick to your goals"
    //let text =  "Goal tracking with real rewards and consequences"

    return(
      <Card id="about-title-section">
        <CardContent>
          <br />
          <Typography id="title-big" variant="display2" gutterBottom align="center">
            CryptoGoals
          </Typography>
          <Typography id="title-small" variant="display1" gutterBottom align="center">
            CryptoGoals
          </Typography>
          <Typography id="title-big" variant="title" gutterBottom align="center">
            {text}
          </Typography>
          <Typography id="title-small" variant="subheading" gutterBottom align="center">
            {text}
          </Typography>
          <br />
          {this.props.onboarding &&
            <div id="center-button">
              <Button color="primary" variant="outlined" onClick={this.props.skip}>
                Skip Intro
              </Button>
            </div>
          }
        </CardContent>
      </Card>
    )
  }
}

export default withStyles(styles)(Title);
