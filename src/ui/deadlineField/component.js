import React, { Component } from 'react';
import { withStyles  } from '@material-ui/core/styles';
import MomentUtils from 'material-ui-pickers/utils/moment-utils';
import moment from 'moment';
import InputLabel from '@material-ui/core/InputLabel';
import DateFnsUtils from "material-ui-pickers/utils/date-fns-utils";
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import DateTimePicker from 'material-ui-pickers/DateTimePicker';
import styles from './deadlineField.css'

class DeadlineFields extends Component {
  constructor(props) {
    super(props)

  	this.state = {
    	selectedDate: null
  	};
 	}

  handleDateChange = date => {
    const data = {
      endTimeHuman: date,
      endTimeUnix: moment(date).unix()
    }
    this.setState({ selectedDate: date });
    this.props.updateFormInfo(data);
  };

  render() {
    const { selectedDate } = this.state;

    return (
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
        <div id="deadlineContainer" className="pickers">
          <DateTimePicker
            label="Deadline"
            fullWidth={true}
            value={selectedDate}
            onChange={this.handleDateChange}
          />
        </div>
      </MuiPickersUtilsProvider>
    );
  }
}

export default withStyles(styles)(DeadlineFields);
