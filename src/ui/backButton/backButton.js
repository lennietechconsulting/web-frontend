import React, { Component } from 'react'
import Icon from '@material-ui/core/Icon';
import { browserHistory } from 'react-router';
import { withStyles  } from '@material-ui/core/styles';
import styles from "./backButton.css";

class BackButton extends Component {
  goHome() {
    browserHistory.push("/")
  }

  render() {
    return (
      <Icon id="backButton" onClick={this.goHome} color="primary"
            aria-label="Back">arrow_back</Icon>
    );
  }
}

export default withStyles(styles)(BackButton);

