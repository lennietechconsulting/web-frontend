import scriptLoader from 'react-async-script-loader'
import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import styles from './styles.css'

class MapComponent extends Component {

  constructor(props) {
    super(props)
    this.google = null
  }

  componentWillReceiveProps ({ isScriptLoaded, isScriptLoadSucceed }) {
    if (isScriptLoaded && !this.props.isScriptLoaded) { // load finished
      if (isScriptLoadSucceed) {
        this.initMap()
      }
    }
  }

  initMap() {
  	var myLatLng = {lat: this.props.lat, lng: this.props.lng};

  	var map = new window.google.maps.Map(document.getElementById('detailsMap'), {
    	zoom: 15,
    	center: myLatLng
  	});

  	var marker = new window.google.maps.Marker({
    	position: myLatLng,
    	map: map,
  	});
  }

  render() {
    return <div id="detailsMap"></div>
  }

}

export default scriptLoader(
  [
    'https://maps.googleapis.com/maps/api/js?key=AIzaSyDxWtCQP3qDWrEMNZQJlPj_wK-KqDVVU74&libraries=places'
  ]
)(withStyles(styles)(MapComponent))
