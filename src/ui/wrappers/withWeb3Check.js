import React, { Component } from 'react'
import Web3 from 'web3'
import Web3Error from '../web3Error/component'
import getWeb3 from '../../util/web3/getWeb3'

// This function takes a component...
function withWeb3Check(WrappedComponent) {
  // ...and returns another component...
  return class extends Component {

    constructor(props) {
      super(props)
      // set to noWeb3, noAccount, or wrongNetwork
      this.state = { web3Error: false }
    }

    checkWeb3ForErrors() {
      this.setState({web3Error: false})


      let web3 = window.web3
      const env = process.env.NODE_ENV

      // Check for web3
      if (typeof web3 === 'undefined') {
        this.setState({web3Error: "noWeb3"})
        return
      }

      web3 = new Web3(web3.currentProvider)

      // Check for account
      if (!web3.eth.accounts.length) {
        this.setState({web3Error: "noAccount"})
      }

      // Check for correct network
      else if (env === "production" && web3.version.network !== "1"){
        this.setState({web3Error: "wrongNetwork"})
      }
    }

    componentWillReceiveProps() {
      this.checkWeb3ForErrors();
    }

    componentDidMount() {
    }

    componentWillUnmount() {
      this.setState({web3Error: false})
    }

    render() {
      // ... and renders the wrapped component with the fresh data!
      // Notice that we pass through any additional props
      return (
        <div>
          { this.state.web3Error &&
            <Web3Error web3Error={this.state.web3Error} />
          }
          <WrappedComponent {...this.props} />
        </div>
      )
    }
  };
}

export default withWeb3Check
