import React, { Component } from 'react'
import { browserHistory } from 'react-router'

function withOnboarding(WrappedComponent) {

  return class extends Component {

    constructor(props) {
      super(props)
      this.state = {}
    }

    checkForOnboarding() {
      if (window.localStorage.getItem("completedOnboarding") === "true") {
        return
      }

      return browserHistory.push('/onboard')
    }

    componentDidMount() {
      this.checkForOnboarding()
    }

    render() {
      return (
        <div>
          <WrappedComponent {...this.props} />
        </div>
      )
    }
  };
}

export default withOnboarding
