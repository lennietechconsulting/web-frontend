import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core/styles';
import AppBarComponent from './ui/appBarComponent/appBarComponent'
import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import styles from "./App.css"
import CookieNotice from "./layouts/cookies/notice/component"
import DocumentMeta from 'react-document-meta';

class App extends Component {

  render() {

    const theme = createMuiTheme({
      palette: {
        primary: {
          light: '#E3F2FD',
          main: '#2196F3',
          dark: '#002884',
          contrastText: '#fff',
        },
        secondary: {
          light: '#ff7961',
          main: '#f44336',
          dark: '#ba000d',
          contrastText: '#000',
        },
      },
    });

    const meta = {
      title: 'CryptoGoals',
      description: 'CryptoGoals is a motivational Dapp that uses Ether to motivate users to stick to their goals. If goals are not completed then users are charged. In the future we plan to reward users in Ether for completing goals.',
      canonical: 'https://cryptogoals.io',
      meta: {
        charset: 'utf-8',
        name: {
          keywords: 'ether,ethereum,dapp,goal,motivation,cryptocurrency,blockchain'
        }
      }
    };

    return (
      <MuiThemeProvider theme={theme}>
        <DocumentMeta {...meta}>
      		<React.Fragment>
        		<CssBaseline />
        		<AppBarComponent/>
          	<CookieNotice />
        		<div>
          		{this.props.children}
        		</div>
      		</React.Fragment>
        </DocumentMeta>
      </MuiThemeProvider>
    );
  }
}

export default withStyles(styles)(App);
