import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'
import $ from "jquery";
import Raven from 'raven-js';
import getWeb3 from './util/web3/getWeb3'
import { initiateData } from './models/common/actions/initiateData'

// Layouts
import App from './AppContainer'
import Dashboard from './layouts/dashboard/DashboardContainer'
import About from './layouts/about/component'
import Contact from './layouts/contact/component'
import Terms from './layouts/Terms/component'
import Privacy from './layouts/privacy/component'
import Cookies from './layouts/cookies/component'
import FAQs from './layouts/faq/component'
import MetamaskGuide from './layouts/metamaskGuide/component'
import Onboard from './layouts/onboard/component'
import NewDeposit from './models/balanceActivity/components/deposit/DepositContainer'
import NewGoal from './models/goal/components/new/newContainer'
import GoalDetails from './models/goal/components/details/container'

// Sentry https://sentry.io
Raven.config('https://c53838f98c4e469c82e5fa6512308e45@sentry.io/1214404', {
  release: '0-0-0',
  environment: 'development-test',
}).install()

// Redux Store
import store from './store'

// Initialize react-router-redux.
const history = syncHistoryWithStore(browserHistory, store)

// Initialize ajax
var baseUrl = process.env.API_URL;
$.ajaxSetup({
  beforeSend: function(xhr, options) {
    options.url = baseUrl + options.url;
  }
})

// Initialize web3 and set in Redux.
getWeb3
.then(results => {
  return initiateData()
})
.catch((err) => {
  console.error('Error in data initiation')
})

ReactDOM.render((
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={App}>
          <IndexRoute component={Dashboard} />
          <Route path="about" component={About} />
          <Route path="contact" component={Contact} />
          <Route path="onboard" component={Onboard} />
          <Route path="faqs" component={FAQs} />
          <Route path="metamask-guide" component={MetamaskGuide} />
          <Route path="deposit" component={NewDeposit} />
          <Route path="terms" component={Terms} />
          <Route path="privacy" component={Privacy} />
          <Route path="cookies" component={Cookies} />
          <Route path="new-goal" component={NewGoal} />
          <Route path="goal-details/:id" component={GoalDetails} />
        </Route>
      </Router>
    </Provider>
  ),
  document.getElementById('root')
)
