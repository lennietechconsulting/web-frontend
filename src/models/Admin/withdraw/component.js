import React, { Component } from 'react'
import contract from 'truffle-contract'
import Button from '@material-ui/core/Button'
import AlarmApp from '../../../../build/contracts/AlarmApp.json'

class AdminWithdraw extends Component {

  withdraw() {
    const web3 = this.props.web3
    const amount = prompt("How much in ether?")
    const alarmApp = contract(AlarmApp)
    alarmApp.setProvider(web3.currentProvider)
    const coinbase = web3.eth.accounts[0]

    alarmApp.deployed().then((instance) => {
      var gasInWei = web3.toWei(0.0003, "ether");
      var gasInGwei = web3.fromWei(gasInWei, "gwei");
      const amountInWei = web3.toWei(amount, "ether");

      web3.eth.getGasPrice((e,rawGasPrice) => {
        let gasPrice
        if (e) {gasPrice = 100000000000}
        gasPrice = +String(rawGasPrice)
        gasPrice = gasPrice + (gasPrice * 0.5)
        const options = {
          from: coinbase,
          gasPrice: gasPrice,
          gas: gasInGwei
        }

        instance.ownerWithdraw(amountInWei, options).then((result) => {
          console.info(result)
        }).catch((error) => {console.error(error)})
      })
    })

  }

  render() {
    const web3 = this.props.web3Instance

    return(
      <Button onClick={this.withdraw.bind(this)}>Withdraw</Button>
    )
  }
}

export default AdminWithdraw;
