const initialState = {}

const accountReducer = (state = initialState, action) => {
  if (action.type === 'GET_ACCOUNT_INFO') {
    return Object.assign({}, state, action.payload)
  }

  return state
}

export default accountReducer
