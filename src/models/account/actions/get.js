import store from '../../../store'
import $ from "jquery";

export const GET_ACCOUNT_INFO = 'GET_ACCOUNT_INFO'

function setAccountInfo(account) {
  return {
    type: GET_ACCOUNT_INFO,
    payload: account
  }
}

export function getAccountInfo() {
  return new Promise((resolve, reject) => {
    let web3 = store.getState().web3.web3Instance

    // Double-check web3's status.
    if (typeof web3 !== 'undefined') {

      web3.eth.getCoinbase((error, coinbase) => {

        if (error) {
          console.error(error);
          reject(error)
        }

        $.ajax({
          url: `/accounts/${coinbase}`,
          dataType: "json"
        })
        .done(function(result) {
          store.dispatch(setAccountInfo(result))
          resolve(result)
        })
        .fail(function(err) {
          console.error(err);
          store.dispatch(setAccountInfo({balance: 0}))
          reject(err)
        });

      })
    } else {
      reject('Web3 is not initialized.');
    }
  })
}
