import contract from 'truffle-contract'
import AlarmApp from '../../../../build/contracts/AlarmApp.json'
import store from '../../../store'

const makeDeposit = ({ depositAmount }) => {
  return new Promise ((resolve, reject) => {
    let web3 = store.getState().web3.web3Instance

    // Double-check web3's status.
    if (typeof web3 !== 'undefined') {

      const alarmApp = contract(AlarmApp)
      alarmApp.setProvider(web3.currentProvider)
      const coinbase = web3.eth.accounts[0]

      if (!coinbase) {
        alert("We could not find your account address.")
        return false
      }

      alarmApp.deployed().then((instance) => {
        const value = web3.toWei(depositAmount, "ether");
        var gasInWei = web3.toWei(0.00003, "ether");
        var gasInGwei = web3.fromWei(gasInWei, "gwei");
        web3.eth.getGasPrice((e,rawGasPrice) => {
          let gasPrice
          if (e) {gasPrice = 100000000000}
          gasPrice = +String(rawGasPrice)
          gasPrice = gasPrice + (gasPrice * 0.25)
          const options = {
            from: coinbase,
            gasPrice: gasPrice,
            gas: gasInGwei,
            value: value
          }
          setTimeout(() => {
            resolve("timeout")
          }, 60000) // return "timeout" after 1 minute of waiting for txn hash

          instance.makeDeposit(options).then((result) => {
            console.info(result)
            resolve(result)
          }).catch((error) => {reject(error)})

        })
      })

    } else {
      reject("Web3 is not initialized")
    }
  })
}

export default makeDeposit
