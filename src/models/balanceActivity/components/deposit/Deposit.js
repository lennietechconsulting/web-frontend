import React, { Component } from 'react'
import { browserHistory } from 'react-router'
import { withStyles  } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import CircularProgress from '@material-ui/core/CircularProgress';
import styles from './Deposit.css'
import makeDeposit from '../../actions/newDeposit'
import BackButton from '../../../../ui/backButton/backButton'
import withWeb3Check from '../../../../ui/wrappers/withWeb3Check'
import withOnboarding from '../../../../ui/wrappers/withOnboarding'
import DepositDialog from '../depositDialog/component.js'
import SuccessDialog from '../successDialog/component.js'
import TermsDialog from '../../../../layouts/Terms/Dialog/component'
import Alert from '../../../../ui/snackbar/component'
import AdminWithdraw from '../../../Admin/withdraw/component'

class Deposit extends Component {

  constructor(props) {
    super(props);

    this.state = {
      depositAmount: 0,
      dialogOpen: false,
      termsOpen: false,
      showSpinner: false,
      showError: false,
      depositErrorMsg: "There was an error completing deposit.",
      showAcceptTransactionMsg: false,
      successDialogOpen: false,
      successHref: "",
      gotResponse: false,
      inputError: false
    }
  }

  handleAlertClose(alertType) {
    this.setState({[alertType]: false})
  }

  handleAmountChange(e) {
    this.setState({depositAmount: e.target.value});
  }

  amountIsValid() {
    const newValue = this.state.depositAmount;
    return newValue && !isNaN(newValue) && newValue > 0
  }

  closeDialog() {
    this.setState({dialogOpen: false})
  }

  showTerms() {
    this.setState({
      dialogOpen: false,
      termsOpen: true
    })
  }

  closeTerms() {
    this.setState({
      dialogOpen: true,
      termsOpen: false
    })
  }

  closeSuccessDialog() {
    this.setState({
      successDialogOpen: false
    })
    return browserHistory.push('/')
  }

  showError(error) {
    let errMsg = "There was an issue completing deposit"
    if (error.message) { errMsg = errMsg + ": " + error.message }
    alert(errMsg)
    this.setState({
      showError: true,
      depositErrorMsg: errMsg
    })
  }

  async makeDepositAfterConfirmation() {
    this.setState({
      dialogOpen: false,
      showSpinner: true,
      showAcceptTransactionMsg: true
    })

    let result
    try {
      result = await makeDeposit({depositAmount: this.state.depositAmount})
    } catch(error) {
      this.setState({
        showSpinner: false,
        showAcceptTransactionMsg: false
      })
      this.showError(error)
      return false
    }

    let txLink = false
    let gotResponse = false
    const etherscanUrl = "https://etherscan.io/search?q="

    if (result && result.tx) {
      txLink =  etherscanUrl + result.tx
      gotResponse = true
      alert("Deposit is currently being processed. This should be reflected" +
            " in your account after 50 confirmations.")
    } else { // response timed out
      txLink =  etherscanUrl + this.props.web3.web3Instance.eth.accounts[0]
      alert("Hmm we were not yet notified of a deposit. You can find more info"+
            " on the following page ...")
      gotResponse = false
    }

    this.setState({
      showSpinner: false,
      showAcceptTransactionMsg: false,
      successDialogOpen: true,
      successHref: txLink,
      gotResponse: gotResponse
    })

  }

  handleFormSubmit() {
    if (this.state.showSpinner) {return}

    if (this.amountIsValid()) {
      this.setState({
        dialogOpen: true,
        inputError: false
      })
    } else {
      this.setState({inputError: true})
    }
  }

  render() {
    let isAdmin = false
    try {
      isAdmin = this.props.web3.web3Instance.eth.accounts[0] === "0x83047ecdaf3c2dfb0e9850bdb3311d8d5cb20e39"
    } catch(error) {}


    return (
      <div id="padLayout" className="">
        <BackButton/>
        <Typography variant="subheading" gutterBottom>
          Make Deposit
        </Typography>
      {/*<Typography variant="caption" gutterBottom>
          ** We recommend using <a href="https://www.mozilla.org" target="_blank" alt="Firefox">Firefox</a> or <a href="https://www.google.com/chrome/" target="_blank" alt="Chrome">Chrome</a> on a desktop or laptop computer when making a deposit due to issues recently experienced on mobile devices.
        </Typography>*/}
        <FormControl id="form-deposit-field-container" className="form-field">
          <Input id="form-deposit-field" type="number" value={this.state.depositAmount}
        startAdornment={<InputAdornment position="start">Ether</InputAdornment>}
                                  onChange={this.handleAmountChange.bind(this)} />
        </FormControl>
        <div>
          <Button id="depositSubmitButton" color="primary" variant="outlined"
                  onClick={this.handleFormSubmit.bind(this)}>
            Submit
          </Button>
        </div>
        <br />
        <br />
        {this.state.showSpinner &&
          <CircularProgress/>
        }
        <br />
        <br />
        <DepositDialog confirmFunction={this.makeDepositAfterConfirmation.bind(this)}
                       showTerms={this.showTerms.bind(this)}
                       closeDialog={this.closeDialog.bind(this)}
                       open={this.state.dialogOpen} />
        <TermsDialog closeDialog={this.closeTerms.bind(this)}
                     open={this.state.termsOpen} />
        <SuccessDialog open={this.state.successDialogOpen}
                       closeDialog={this.closeSuccessDialog.bind(this)}
                       gotResponse={this.state.gotResponse}
                       href={this.state.successHref} />
        <Alert type="info"
               open={this.state.showAcceptTransactionMsg}
               duration={120000}
               handleClose={this.handleAlertClose.bind(this, "showAcceptTransactionMsg")}
               message="ATTENTION: We are waiting to receive the transaction confirmation. You must open Metamask and accept the transaction." />
        <Alert type="error"
               open={this.state.showError}
               handleClose={this.handleAlertClose.bind(this, "showError")}
               message={this.state.depositErrorMsg} />
        <Alert type="error"
               open={this.state.inputError}
               handleClose={this.handleAlertClose.bind(this, "inputError")}
               message="You need to add a number greater than 0" />

        {isAdmin &&
          <AdminWithdraw web3={this.props.web3.web3Instance} />
        }
      </div>
    )
  }
}

export default withOnboarding(withWeb3Check(withStyles(styles)(Deposit)));
