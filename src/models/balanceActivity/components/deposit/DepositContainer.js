import { connect } from 'react-redux'
import Deposit from './Deposit'

const mapStateToProps = (state, ownProps) => {
  return {
    web3: state.web3
  }
}

const mapDispatchToProps = (dispatch) => {
  return {}
}

const DepositContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Deposit)

export default DepositContainer
