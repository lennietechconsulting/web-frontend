import React, { Component } from 'react'
import { Link } from 'react-router'
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button'
import DialogActions from '@material-ui/core/DialogActions';
import withMobileDialog from '@material-ui/core/withMobileDialog';
import AppBarComponent from '../../../../ui/appBarComponent/appBarComponent';

class SuccessDialog extends Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    let title
    let text1
    let text2
    let text3
    let btnText
    if (this.props.gotResponse) {
      title = "Deposit Successful"
      text1 = "Deposit should appear in your balance after the transaction has received 50 confirmations."
      text3 = "You can check how many confirmations have passed by clicking on link below."
      btnText = "View Transaction"
    } else {
      title = "We have not yet received a deposit notification"
      text1 = "Possible reasons are not accepting the transaction in metamask or the gas price entered was too low. Try increasing gas price to increase processing time."
      //text2 = "We have also experienced issues depositing on a mobile device so we recommend using Chrome or Firefox on a desktop or laptop computer when making a deposit."
      text3 = "You can check if your account has processed the transaction by clicking on the link below. If the transaction is successful, the deposit should appear in your account after the transaction has received 50 confirmations. "
      btnText = "View Account Info"
    }

    return(
      <div>
        <Dialog
          fullScreen={this.props.fullScreen}
          open={this.props.open}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          { this.props.fullScreen &&
            <AppBarComponent />
          }
          <DialogTitle align="center" id="alert-dialog-title">{title}</DialogTitle>
          <DialogContent>
            <div>
              <DialogContentText id="alert-dialog-description">
                {text1}
              </DialogContentText>
              <br />
              {text2 &&
                <div>
                  <DialogContentText id="alert-dialog-description">
                    {text2}
                  </DialogContentText>
                  <br />
                </div>
              }
              {this.props.href &&
                <div>
                  <DialogContentText id="alert-dialog-description">
                    {text3}
                  </DialogContentText>
                  <br />
                  <Button href={this.props.href}
                          target="_blank"
                          fullWidth={true}
                          size="small"
                          color="primary"
                          variant="outlined">
                    {btnText}
                  </Button>
                </div>
              }
            </div>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.props.closeDialog} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

export default withMobileDialog()(SuccessDialog);
