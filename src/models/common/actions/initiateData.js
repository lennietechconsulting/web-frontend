import { getAccountInfo } from '../../account/actions/get'
import { getGoals } from '../../goal/actions/getList'

export function initiateData() {
  return getAccountInfo()
    .then(() => {
      return getGoals()
    })
}
