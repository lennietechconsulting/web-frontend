import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import userReducer from './user/userReducer'
import accountReducer from './models/account/reducer'
import goalReducer from './models/goal/reducer'
import web3Reducer from './util/web3/web3Reducer'

const reducer = combineReducers({
  routing: routerReducer,
  user: userReducer,
  goals: goalReducer,
  web3: web3Reducer,
  account: accountReducer
})

export default reducer
