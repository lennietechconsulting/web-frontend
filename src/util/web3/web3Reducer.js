const initialState = {
  web3Instance: null,
  checked: false
}

const web3Reducer = (state = initialState, action) => {
  if (action.type === 'WEB3_INITIALIZED') {
    return Object.assign({}, state, {
      web3Instance: action.payload.web3Instance,
      checked: action.payload.checked,
    })
  }

  if (action.type === 'NO_WEB3') {
    return Object.assign({}, state, {
      checked: true,
    })
  }

  return state
}

export default web3Reducer
