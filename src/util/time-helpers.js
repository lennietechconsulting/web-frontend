import moment from 'moment'

exports.formatTime = (time) => {
  const _moment = moment
  return moment(time).format("ddd MMM h:mm a")
}
