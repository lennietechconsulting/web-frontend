import store from '../store'

exports.signSig = (msgParams, from, cb) => {

    const web3 = store.getState().web3.web3Instance

    web3.currentProvider.sendAsync({
    	method: 'eth_signTypedData',
    	params: [msgParams, from],
    	from: from,
  	}, (err, result) => {

    	if (err) return cb(err);
    	if (result.error) {
    	  return cb(result.error);
    	}
    	return cb(null, result.result);

  	})

  }


